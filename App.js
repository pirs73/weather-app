import React, { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { Alert, StyleSheet, Text, View } from 'react-native';
import * as Location from 'expo-location';
import axios from 'axios';
import { WEATHER_API_KEY } from '@env';
import Loading from './Loading';

export default function App() {
    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();

            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            const {
                coords: { latitude, longitude },
            } = location;

            const getWeather = async (lat, lon) => {
                const { data } = await axios.get(
                    `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${WEATHER_API_KEY}`,
                );

                console.log(data);
            };

            getWeather(latitude, longitude);
            setIsLoading(false);
            setLocation(location);
        })();
    }, []);

    let text = 'Получение погоды...';

    if (errorMsg) {
        text = errorMsg;
    } else if (location && !isLoading) {
        // const {
        //     coords: { latitude, longitude },
        // } = location;
        // /** TODO: Сделать запрос к API погоды */
        // text = `Широта: ${JSON.stringify(latitude)} Долгота: ${JSON.stringify(
        //     longitude,
        // )}`;
    }

    return <>{isLoading ? <Loading text={text} /> : <Loading text={text} />}</>;
}
